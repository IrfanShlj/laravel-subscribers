<?php

use Illuminate\Support\Facades\Route;

Route::post('subscriber', 'SubscriberController@store')->name('store');
Route::delete('delete', 'SubscriberController@delete')->name('delete');
