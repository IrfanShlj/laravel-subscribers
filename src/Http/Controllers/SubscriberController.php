<?php

namespace Runit\Subscribers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Routing\Controller;
use Runit\Subscribers\Subscriber;
use Illuminate\Support\Facades\Storage;
use Runit\Subscribers\Events\NewSubscriber;
use Runit\Subscribers\Http\Requests\StoreSubscriberRequest;
use Runit\Subscribers\Http\Requests\DeleteSubscriberRequest;

class SubscriberController extends Controller
{
    public function store(StoreSubscriberRequest $request)
    {
        Subscriber::create($request->all());

        return back()->with('subscribed', 'You are successfully subscribed to our list!');
    }

    public function delete(DeleteSubscriberRequest $request)
    {
        $res=Subscriber::where('id',$request->subscriber()->id)->delete();
        if($res)
          return back()->with('unsubscribed', 'You are successfully unsubscribed from our list!');
        else {
          return back();
        }
    }
}
