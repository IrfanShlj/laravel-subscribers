<?php

namespace Runit\Subscribers\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Routing\Controller;
use Runit\Subscribers\Subscriber;
use Illuminate\Support\Facades\Storage;
use Runit\Subscribers\Events\NewSubscriber;
use Runit\Subscribers\Http\Requests\StoreSubscriberRequest;
use Runit\Subscribers\Http\Requests\DeleteSubscriberRequest;
use Runit\Subscribers\Events\SubscriberCreated;

class SubscriberController extends Controller
{
    public function store(StoreSubscriberRequest $request)
    {
        $subscriber = Subscriber::create($request->all());

        return response()->json([
            'created' => true,
        ], 201);
    }
    public function delete(DeleteSubscriberRequest $request)
    {
      $res=Subscriber::where('id',$request->subscriber()->id)->delete();

        if($res)return response()->json([
            'deleted' => true,
        ], 201);
        else {
          return response()->json([
              'deleted' => false,
          ], 201);
        }
    }
}
