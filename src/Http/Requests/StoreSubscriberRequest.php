<?php

namespace Runit\Subscribers\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class StoreSubscriberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $product = $this->product;
      $id_product = $this->id_product;
        return [
            'email' => [
        'required','email','max:255',
        Rule::unique('subscribers','email')->where(function ($query) use($product,$id_product) {
           return $query->where('product', $product)->where('id_product', $id_product);
        })
            ],
            'product'=>'nullable|max:255',
            'id_product'=>'nullable',
            'length'=>'nullable|max:2',

        ];
    }
}
