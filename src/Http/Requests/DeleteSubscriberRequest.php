<?php

namespace Runit\Subscribers\Http\Requests;

use Runit\Subscribers\Subscriber;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class DeleteSubscriberRequest extends FormRequest
{
    //error redirect route that should be defined in the app.
    protected $redirect="error";
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
       $product = $this->product;
       $id_product = $this->id_product;
         return [
           'product'=>['required','max:255'],
           'id_product'=>['required','numeric'],
             'email' => [
         'required','email','max:255',
         Rule::exists('subscribers','email')->where(function ($query) use($product,$id_product) {
            return $query->where('product', $product)->where('id_product', $id_product);
         })
             ],
             'length'=>'nullable|max:2'

         ];
     }

    public function subscriber()
    {
        return Subscriber::where('email', $this->input('email'))->where('product', $this->input('product'))->where('id_product', $this->input('id_product'))->firstOrFail();
    }
}
