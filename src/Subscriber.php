<?php

namespace Runit\Subscribers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Runit\Subscribers\Events\SubscriberCreated;
use Runit\Subscribers\Events\SubscriberDeleted;

class Subscriber extends Model
{
    use Notifiable;

    protected $table = 'subscribers';

    protected $fillable = [
        'email','product','id_product','length'
    ];

    protected $dispatchesEvents = [
        'created' => SubscriberCreated::class,
        'deleted' => SubscriberDeleted::class,
    ];

  }
