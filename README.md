# Manage Multi-object Newsletter Subscribers in Laravel

This is a fork of mydnic/laravel-subscribers to allow for multi-layer product subscription. Originally, the package allowed for a single-list email subscription. In this package we have expanded the original functionality to be able to store subscriptions to objects in different tables in the application. Furthermore, we have added an option to specify frequency of the email notifications.

## Installation

Until the package is avalable on the official packagist repository, you can include the git repository in composer.json like this if you have multiple repositories:
  ```bash
  "repositories": [
        {
            "type": "composer",
            "url": "https://larapack.io"
        },
      {
        "type": "vcs",
        "url": "https://gitlab.com/IrfanShlj/laravel-subscribers"
   }
    ]
 ```   
 or if you don't have any repositories specified:
 ```bash
 "repositories": 
      {
        "type": "vcs",
        "url": "https://gitlab.com/IrfanShlj/laravel-subscribers"
   },
```  
Then, you may use Composer to Install Laravel Subscribers:

```bash
composer require runit/laravel-subscribers
```

The package will automatically register itself

You then must publish the migration with:

```bash
php artisan vendor:publish --provider="Runit\Subscribers\SubscribersServiceProvider" --tag="subscribers-migrations"
```
And you can run the single migration as follows:
```bash
php artisan migrate --path=/database/migrations/2018_01_01_000000_create_subscribers_table.php
```
## Usage

In your view, you simply need to add a form that you can customize the way you want. For example, the form below uses the currently logged in user's information to check if the user is subscribed to the current course and shows the appropriate Subscribe or Unsubscribe button. If there is no user logged in, then only subscribe is available.  

```blade
<div class="row col-md-12">
@if(Auth::user() && Runit\Subscribers\Subscriber::where('email', Auth::user()->email)->where('product','course')->where('id_product',$course->id)->first())
<form action="{{ route('subscribers.delete') }}" method="post">
  @method('DELETE')
  @csrf
    <input hidden type="email" name="email" value="{{Auth::user()->email}}">
    <input hidden type="text" name="product" value="course">
    <input hidden type="text" name="id_product" value="{{$course->id}}">
    <input type="submit" value="Unsubscribe">
</form>
@else
<form action="{{ route('subscribers.store') }}" method="post">
    @csrf
    @guest
    <input type="email" name="email">
    @endguest
    @auth
    <input hidden type="email" name="email" value="{{Auth::user()->email}}">
    @endauth
    <input hidden type="text" name="product" value="course">
    <input hidden type="text" name="id_product" value="{{$course->id}}">
    <input hidden type="text" name="length" value="d">
    <input type="submit" value="Subscribe">
</form>
@endif
</div>
@if (session('subscribed'))
<div class="row col-md-11">
    <div class="alert alert-success">
        {{ session('subscribed') }}
    </div>
</div>
@endif
@if (session('unsubscribed'))
<div class="row col-md-11">
    <div class="alert alert-success">
        {{ session('unsubscribed') }}
    </div>
</div>
```


### Delete
Simply provide this link to your subscribers. This can be done in the email template:

```blade
<a href="{{ route('subscribers.delete', ['email' => $subscriber->email,'product' => $subscriber->product,'id_product' => $subscriber->id_product]) }}">unsubscribe</a>
```

This will generate a link like `/subscribers/delete?email=email@example.com&product=course&id_product=1`
